#include <QImage>
#include <QVector>
#include <QColor>

#include <iostream>

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        std::cout << "ERROR: invalid program arguments\nExample: qpalettemaker inputfile.png outputfile.bmp" << std::endl;
        return -1;
    }

    QVector<QColor> colors;
    QImage input;
    if( !input.load(argv[1]) )
    {
        std::cout << "ERROR: input file not found" << std::endl;
        return -2;
    }

    for(auto x = 0; x < input.width(); ++x)
    {
        for(auto y = 0; y < input.height(); ++y)
        {
            auto p = input.pixelColor(x,y);
            p.setAlpha(255);
            bool found = false;
            for(auto& i : colors)
            {
                if(i == p)
                {
                    found = true;
                }
            }
            if(!found)
            {
                colors.push_back(p);
            }
        }
    }


    QImage output = input.scaled(colors.size(), 1);
    for(auto x = 0; x < colors.size(); ++x)
    {
        output.setPixelColor(x,0,colors[x]);
    }


    if( output.save(argv[2]) )
    {
        std::cout << "palette written to " << argv[2] << std::endl;
        return 0;
    }
    else
    {
        std::cout << "ERROR: could not write output file" << std::endl;
        return -3;
    }

}
